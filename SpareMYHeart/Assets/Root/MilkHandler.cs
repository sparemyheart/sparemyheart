﻿using UnityEngine;
using System.Collections;

public class MilkHandler : MonoBehaviour
{
    void OnMouseOver ()
    {
        if ( Input.GetMouseButtonDown( 0 ) && !GameManager.gm.foundMilk)
        {
            GameManager.gm.foundOrder.Add( "Milk" );
            GameManager.gm.foundMilk = true;
            Destroy( gameObject );
        }
    }
}
