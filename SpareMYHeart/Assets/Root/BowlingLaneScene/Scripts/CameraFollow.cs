﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
	//setting up the variables for the camera
	private Vector2 velocity;

	public GameObject ballSenpai;

	// Use this for initialization
	void Start ()
	{
		//finding the player so that we can allow the camera to follow them
		ballSenpai = GameObject.FindGameObjectWithTag ("Player");
	}

	// Update is called once per frame
	void Update ()
	{
		transform.position = new Vector3 (0, 5, ballSenpai.transform.parent.position.z - 5.0f);
	}

}
