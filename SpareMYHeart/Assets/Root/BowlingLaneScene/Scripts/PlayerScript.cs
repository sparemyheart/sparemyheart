﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{
	[SerializeField]
	Transform[] endingPoint;
	int index = 0;

	[SerializeField][Range (-10.0f, 10.0f)]
	private float
		x,
		y,
		z,
		speed = 10;

	public int Index {
		get {
			return index;
		}
		set {
			if (value >= endingPoint.Length) {
				index = endingPoint.Length - 1;
			} else if (value < 0) {
				index = 0;
			} else {
				index = value;
			}
		}
	}

	[SerializeField]


	// Use this for initialization
	void Start ()
	{
	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		//float distance = Vector3.Distance (transform.parent.position, endingPoint [Index].position);
		//if (distance < 0.01f) {
		//	Index++;

		//} else {
		//	transform.parent.position = Vector3.Lerp (transform.parent.position, endingPoint [Index].position, speed / distance);

		//	transform.parent.Rotate (x, y, z);

		//}
	}
}
