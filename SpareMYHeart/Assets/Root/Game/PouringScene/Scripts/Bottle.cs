﻿using UnityEngine;
using System.Collections;

public class Bottle  : MonoBehaviour {
    private Color _color;
    private Vector3 _size;
    private int _position;
    private int _shelf;

    private bool _wasSelected = false;

    private bool _isMilk = false;
    private bool _isVodka = false;
    private bool _isKahlua = false;

    public GameObject bottleObjInstance;

    public Bottle(Color color, Vector3 size, int position, int shelf)
    {
        _color = color;
        _size = size;
        _position = position;
        _shelf = shelf;
    }

    //INITIAL SETTERS FOR INGREDIENTS
    public void SetAsMilk()
    {
        _isMilk = true;
    }

    public void SetAsVodka()
    {
        _isVodka = true;
    }

    public void SetAsKahlua()
    {
        _isKahlua = true;
    }

    //CHECK TO SEE STATE OF BOTTLE - TRIGGERED BY CLICK EVENT
    public void SelectBottle()
    {
        _wasSelected = true;

        if(_isMilk)
        {
            //found milk raise some shit
        }
        else if( _isVodka )
        {
            //found vodka raise some shit
        }
        else if (_isKahlua)
        {
            //found kahlua raise some shit
        }
    }

    public override string ToString ()
    {
        if ( _isMilk )
            return "milk";
        else if ( _isVodka )
            return "vodka";
        else if ( _isKahlua )
            return "kahlua";

        return "regular";
    }

 
}
