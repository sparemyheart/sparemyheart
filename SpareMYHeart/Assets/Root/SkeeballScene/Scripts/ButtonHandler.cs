﻿using UnityEngine;
using System.Collections;

public class ButtonHandler : MonoBehaviour {
    public GameObject canvasElement;

    public void OnClick()
    {
        canvasElement.SetActive( false );
        Cannon.s.canShoot = true;
    }
}
