﻿using UnityEngine;
using System.Collections;

public class LightBend : MonoBehaviour {

    [SerializeField]
    Transform
        one,
        two;

    [SerializeField]
    float speed;

	// Update is called once per frame
	void FixedUpdate () {
        transform.rotation = Quaternion.Lerp( one.rotation, two.rotation, Mathf.Sin(Time.time*speed)/2+0.5f );
	}
}
