﻿using UnityEngine;
using System.Collections;

public class Skeeball : MonoBehaviour {

    void OnTriggerEnter ( Collider otherColl )
    {
        Destroy(gameObject,0.3f);
        CheckShotConditions(otherColl);
        Cannon.s.canShoot = true;
    }
    void Start()
    {
        Destroy( gameObject, 10.0f );
    }
    void CheckShotConditions ( Collider otherColl )
    {
        if ( otherColl.name == "25pts" )
        {
            print( "YOU SCORED 25 POINTS" );
            ScoreManager.currentScore += 25;
        }
        else if ( otherColl.name == "10pts" )
        {
            print( "YOU SCORE 10 POINTS" );
            ScoreManager.currentScore += 10;
        }
        else if ( otherColl.name == "100pts-LEFT" || otherColl.name == "100pts-RIGHT" )
        {
            print( "YOU SCORE 100 POINTS" );
            ScoreManager.currentScore += 100;
        }
        else if ( otherColl.name == "50pts" )
        {
            print( "YOU SCORE 50 POINTS" );
            ScoreManager.currentScore += 50;
        }
        else
        {
            print( "YOU MISSED!" );
        }
    }
}
