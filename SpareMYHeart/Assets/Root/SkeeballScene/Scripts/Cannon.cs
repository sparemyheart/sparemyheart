﻿using UnityEngine;
using System.Collections;

public class Cannon : MonoBehaviour {
    public static Cannon s;
    public bool canShoot;
    public bool gameFinished;

    [SerializeField]
    [Range(1, 10)]
    public int shotAllowance = 10;

    private int shotsTaken = 0;

    [SerializeField]
    GameObject skeeball;

    [SerializeField]
    Transform spawnPos;

    [SerializeField]
    [Range(500.0f, 3000.0f)]
    float force;

	void Start () {
        s = this;
        canShoot = false;
        gameFinished = false;
	}
	
	void Update () {
        if(Input.GetButtonDown("Fire1"))
        {
            Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );

            /*ensures players cant fucking cheat */
            float shotEntropyOffset = Random.Range(0.7f, 1.0f);

            if(Physics.Raycast( ray ) )
            {
                print( Input.mousePosition.x.ToString() + " " + Input.mousePosition.y.ToString() );

                if ( canShoot && !gameFinished)
                {
                    canShoot = !canShoot;
                    ( Instantiate( skeeball, spawnPos.position, Quaternion.identity ) as GameObject ).GetComponent<Rigidbody>().AddForce( new Vector3( -Input.mousePosition.y*2, 0, Input.mousePosition.x/(3* shotEntropyOffset )  - 450) );
                    shotsTaken++;
                }
                else
                {
                    return;
                }

                if ( shotsTaken == shotAllowance )
                {
                    gameFinished = true;
                    print( ScoreManager.currentScore );
                    Invoke( "gameOver", 5.0f );
                }
            }
        }
	}

    void gameOver()
    {
        print( "Game over!" );
    }
}
