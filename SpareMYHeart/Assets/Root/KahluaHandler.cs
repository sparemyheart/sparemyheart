﻿using UnityEngine;
using System.Collections;

public class KahluaHandler : MonoBehaviour {
    void OnMouseOver ()
    {
        if ( Input.GetMouseButtonDown( 0 ) && !GameManager.gm.foundKahlua )
        {
            GameManager.gm.foundOrder.Add( "Kahlua" );
            GameManager.gm.foundKahlua = true;
            Destroy( gameObject );
        }
    }
}

