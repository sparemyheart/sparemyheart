﻿using UnityEngine;
using System.Collections;

public class VodkaHandler : MonoBehaviour {
    void OnMouseOver ()
    {
        if ( Input.GetMouseButtonDown( 0 ) && !GameManager.gm.foundVodka )
        {
            GameManager.gm.foundOrder.Add( "Vodka" );
            GameManager.gm.foundVodka = true;
            Destroy( gameObject );
        }
    }
}

